package granimation.com.foodme;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Sindre on 27.09.2014.
 */
public class GridImageViewAdapter extends BaseAdapter {
    private Context mContext;
    public int imageSizeX;
    public int imageSizeY;
    public ArrayList<String> urlPage = new ArrayList<String>();
    public ArrayList<String> urlImage = new ArrayList<String>();

    // Constructor
    public GridImageViewAdapter(Context c, int x, int y) {
        mContext = c;
        imageSizeX = x;
        imageSizeY = y;
    }

    public int getCount() {
        return urlImage.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(imageSizeX, imageSizeY));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(2, 2, 2, 2);
        } else {
            imageView = (ImageView) convertView;
        }

        if (position < urlImage.size()) {
            Picasso.with(mContext).load(urlImage.get(position)).resize(150, 150).centerCrop().into(imageView);
            //Picasso.with(mContext).load("http://hig.no/design/HiG_v4/images/menu/HiG_logo_RGB.png").resize(150, 150).centerCrop().into(imageView);
        }

        return imageView;
    }
}
