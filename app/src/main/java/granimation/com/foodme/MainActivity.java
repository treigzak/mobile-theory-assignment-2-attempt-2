package granimation.com.foodme;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

@SuppressLint("SetJavaScriptEnabled")
public class MainActivity extends Activity implements SensorEventListener {
    //Variables
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 400;
    private String tab1Name;
    private String tab2Name;
    private String tab3Name;
    private String tab4Name;
    private String recipeURL = "";
    private String recipeID = "";
    private String recipeImageSubstring1 = "http://hemb.priv.no/privat/randomDinner.php?dinnerID=";
    private String recipeImageSubstring2 = "&showImage=";
    private final String RANDOM_DINNER_URL = "http://hemb.priv.no/privat/randomDinner.php";
    private boolean inFoodTab = true;
    private boolean canShake = true;
    private boolean pictureSet = false;
    private boolean recipeNameSet = false;
    private boolean instructionsSet = false;
    private boolean ingredientSet = true;
    GridImageViewAdapter historyImageViewAdapter = new GridImageViewAdapter(this, 150, 150);
    GridImageViewAdapter favouritesImageViewAdapter = new GridImageViewAdapter(this, 300, 300);
    DatabaseHandler favouriteDB;
    final ArrayList<EditText> ingredientInputFields = new ArrayList<EditText>();


    private boolean pictureInProgress = false;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;

    //Objects
    private TextView txtShakeNotifier;
    private ImageButton btnAddFavourite;
    private ImageButton btnGallery;
    private ImageButton btnCamera;
    private ImageView imgSubmit;
    private WebView webRecipe;
    private GridView grdHistory;
    private GridView grdFavourites;
    private TabHost tabHost;
    private Button btnSubmitRecipe;
    private Button btnReset;
    private EditText txtRecipeName;
    private EditText txtInstructions;
    private LinearLayout ingredientLinearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        favouriteDB = new DatabaseHandler(getApplicationContext());

        // Set up tab layouts and button functionality
        setupTabs();

        //Lock to portrait orientation due to shaking
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Set up accelerometer sensor
        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);

        // Load favourites from database to favourites tab
        addFavouritesFromDatabase();

    }

    public void addFavouritesFromDatabase(){
        if(favouriteDB.getCount() > 0){
            Vector<String> favourites = new Vector<String>();
            favourites = favouriteDB.getAll();
            for(int i = 0; i < favourites.size(); i++){
                String newRecipe = favourites.get(i);
                favouritesImageViewAdapter.urlPage.add(0, newRecipe);
                int subUrlIndex = newRecipe.indexOf('=');
                String favouriteID = newRecipe.substring(subUrlIndex + 1, newRecipe.length());
                String imageURL = recipeImageSubstring1 + favouriteID + recipeImageSubstring2 + favouriteID;
                favouritesImageViewAdapter.urlImage.add(0, imageURL);
            }
        }
    }

    //Get sensor data
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (!inFoodTab || !canShake) {
            return;
        }
        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();


            if ((curTime - lastUpdate) > 200) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;


                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {
                    getNewRecipe(RANDOM_DINNER_URL, true);
                }

                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void handleFavouriteIcon(int foundAtIndex) {
        if (foundAtIndex >= 0) {
            btnAddFavourite.setImageResource(android.R.drawable.btn_star_big_on);
        } else {
            btnAddFavourite.setImageResource(android.R.drawable.btn_star_big_off);
        }
    }

    public int findInFavorites(String url) {
        int foundAtIndex = -1;
        for (int i = 0; i < favouritesImageViewAdapter.urlPage.size(); i++) {
            if (favouritesImageViewAdapter.urlPage.get(i).equals(url)) {
                foundAtIndex = i;
                break;
            }
        }
        return foundAtIndex; //Will return -1 if not found
    }

    public void getNewRecipe(String newURL, boolean saveInHisory) {
        //If this is not the first refresh, add previous page to the log


        String summary = "<html><body><h1><center>Loading...</center></h1></body></html>";
        webRecipe.loadData(summary, "text/html", null);


        if (!recipeURL.equals("") && saveInHisory) {
            historyImageViewAdapter.urlPage.add(0, recipeURL);
            String imageURL = recipeImageSubstring1 + recipeID + recipeImageSubstring2 + recipeID;
            historyImageViewAdapter.urlImage.add(0, imageURL);
        }

        // Load the webpage when the device is connected to the network
        if (hasConnectivity()) {
            // Load the webpage when the device is connected to the network
            webRecipe.loadUrl(newURL);
            // The standard webpage redirects to a random recipe, and this code keeps it from starting up the browser.
            // Does NOT launch right after the loadUrl call!! :(
            webRecipe.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url){
                    recipeURL = url;
                    view.loadUrl(recipeURL);

                    // Get th recipe ID
                    int subUrlIndex = recipeURL.indexOf('=');
                    recipeID = recipeURL.substring(subUrlIndex +1, recipeURL.length());
                    
                    // Check if this recipe is favourited and set correct star icon accordingly
                    int foundAtIndex = findInFavorites(recipeURL);
                    handleFavouriteIcon(foundAtIndex);


                    return false; // Return false so it is not handled by default action
                }
            });
            if (!newURL.equals(RANDOM_DINNER_URL)) {
                recipeURL = newURL;
                int subUrlIndex = recipeURL.indexOf('=');
                recipeID = recipeURL.substring(subUrlIndex +1, recipeURL.length());

                int foundAtIndex = findInFavorites(recipeURL);
                handleFavouriteIcon(foundAtIndex);
            }

            txtShakeNotifier.setVisibility(View.INVISIBLE);
            webRecipe.setVisibility(View.VISIBLE);

        } else {
            // set text to saved instance from the file, and add a message about the network
            txtShakeNotifier.setText(getString(R.string.errorNoNetworkConnection));
            txtShakeNotifier.setVisibility(View.VISIBLE);
            webRecipe.setVisibility(View.INVISIBLE);
        }
    }

    public boolean hasConnectivity() {
        ConnectivityManager connect = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connect != null) {
            NetworkInfo[] information = connect.getAllNetworkInfo();
            if (information != null) {
                for (int x = 0; x < information.length; x++) {
                    if (information[x].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void setupTabs(){
        //Set up tabs
        tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        tab1Name = getString(R.string.tabFood);
        tab2Name = getString(R.string.tabFavourites);
        tab3Name = getString(R.string.tabLog);
        tab4Name = getString(R.string.tabSubmit);

        TabHost.TabSpec tabSpec = tabHost.newTabSpec(tab1Name);
        tabSpec.setContent(R.id.tabRandom);
        tabSpec.setIndicator(tab1Name);
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(tab2Name);
        tabSpec.setContent(R.id.tabFavourites);
        tabSpec.setIndicator(tab2Name);
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(tab3Name);
        tabSpec.setContent(R.id.tabHistory);
        tabSpec.setIndicator(tab3Name);
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(tab4Name);
        tabSpec.setContent(R.id.tabSubmit);
        tabSpec.setIndicator(tab4Name);
        tabHost.addTab(tabSpec);

        //Set up objects
        ingredientLinearLayout = (LinearLayout)findViewById(R.id.ingredientLinearLayout);

        webRecipe = (WebView) findViewById(R.id.webRecipe);
        webRecipe.getSettings().setJavaScriptEnabled(true);
        webRecipe.setBackgroundColor(0);
        grdHistory = (GridView) findViewById(R.id.grdHistory);
        grdHistory.setAdapter(historyImageViewAdapter);

        grdFavourites = (GridView) findViewById(R.id.grdFavourites);
        grdFavourites.setAdapter(favouritesImageViewAdapter);

        ingredientInputFields.add(new EditText(this));
        ingredientInputFields.get(0).setHint(getString(R.string.ingredientEmptyFieldHint));
        ingredientLinearLayout.addView(ingredientInputFields.get(0));

        ImageButton addIngredientBtn = (ImageButton)findViewById(R.id.addIngredientButton);
        final ImageButton removeIngredientBtn = (ImageButton)findViewById(R.id.removeIngredientButton);

        txtShakeNotifier = (TextView) findViewById(R.id.txtShakeNotifier);
        btnAddFavourite = (ImageButton) findViewById(R.id.btnAddFavourite);
        btnGallery = (ImageButton) findViewById(R.id.btnGallery);
        imgSubmit = (ImageView) findViewById(R.id.imgSubmit);
        btnReset = (Button) findViewById(R.id.btnReset);
        btnSubmitRecipe = (Button) findViewById(R.id.submitRecipeBtn);
        ImageButton btnGenerate = (ImageButton) findViewById(R.id.btnGenerate);
        txtRecipeName = (EditText) findViewById(R.id.recipeName);
        txtInstructions = (EditText) findViewById(R.id.recipeInstructions);
        btnCamera = (ImageButton) findViewById(R.id.btnCamera);


        //Set up onclick listeners
        // Get a random recipe
        txtShakeNotifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getNewRecipe(RANDOM_DINNER_URL, true);
            }
        });

        // Add a recipe to favourites
        btnAddFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (webRecipe.getUrl() == null) {
                    return;
                }

                //Check if recipe is previously favourited
                int foundAtIndex = findInFavorites(webRecipe.getUrl());

                String toastText = "";
                if (foundAtIndex >= 0) {
                    favouriteDB.remove(favouritesImageViewAdapter.urlPage.get(foundAtIndex));
                    favouritesImageViewAdapter.urlPage.remove(foundAtIndex);
                    favouritesImageViewAdapter.urlImage.remove(foundAtIndex);
                    btnAddFavourite.setImageResource(android.R.drawable.btn_star_big_off);

                    toastText = getString(R.string.recipeRemovedFromFavouritesSuccess);
                } else {
                    favouriteDB.add(webRecipe.getUrl());
                    favouritesImageViewAdapter.urlPage.add(0, webRecipe.getUrl());
                    String imageURL = recipeImageSubstring1 + recipeID + recipeImageSubstring2 + recipeID;
                    favouritesImageViewAdapter.urlImage.add(0, imageURL);
                    btnAddFavourite.setImageResource(android.R.drawable.btn_star_big_on);
                    toastText = getString(R.string.recipeAddedToFavouritesSuccess);
                }
                Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_SHORT).show();
            }
        });

        // Get a random recipe
        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getNewRecipe(RANDOM_DINNER_URL, true);
            }
        });

        // Recipe name is set if there is something in the recipe name text.
        txtRecipeName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                recipeNameSet = (String.valueOf(txtRecipeName.getText()).trim().length() > 0);
                testIfCanSubmit();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        // Instructions are set if there is something in the instructions text.
        txtInstructions.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                instructionsSet = (String.valueOf(txtInstructions.getText()).trim().length() > 0);
                testIfCanSubmit();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        // If an item in the log tab was clicked
        grdHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                tabHost.setCurrentTab(0);
                getNewRecipe(historyImageViewAdapter.urlPage.get(position), true);
                //Log.d("appDebug", "Recipe id: " + position + " | " + historyImageViewAdapter.urlPage.get(position));
                //Toast.makeText(getApplicationContext(), "Image: " + gridImageViewAdapter.historyImage.get(position), Toast.LENGTH_SHORT).show();
            }
        });

        // If an item in the fav tab was clicked
        grdFavourites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                tabHost.setCurrentTab(0);
                getNewRecipe(favouritesImageViewAdapter.urlPage.get(position), true);
                //Log.d("appDebug", "Recipe id: " + position + " | " + favouritesImageViewAdapter.urlPage.get(position));
                //Toast.makeText(getApplicationContext(), "Image: " + gridImageViewAdapter.historyImage.get(position), Toast.LENGTH_SHORT).show();
            }
        });

        // Make sure to only use the accelerometer when in the Food! tab, and refresh gridviews accordingly
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if (tab1Name.equals(tabId)) {
                    inFoodTab = true;
                }
                else if (tab2Name.equals(tabId)) {
                    inFoodTab = false;
                    grdFavourites.invalidateViews();
                }
                else if (tab3Name.equals(tabId)) {
                    inFoodTab = false;
                    grdHistory.invalidateViews();
                }
                else if (tab4Name.equals(tabId)) {
                    inFoodTab = false;
                }
            }
        });

        // Add new edittext line to write down another ingredient in
        addIngredientBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ingredientInputFields.add(new EditText(ingredientLinearLayout.getContext()));
                ingredientInputFields.get(ingredientInputFields.size() - 1).setHint(getString(R.string.ingredientEmptyFieldHint));
                ingredientInputFields.get(ingredientInputFields.size() - 1).setTextColor(Color.BLACK);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ingredientLinearLayout.addView(ingredientInputFields.get(ingredientInputFields.size() - 1));
                    }
                });
                if (ingredientInputFields.size() > 0) {
                    removeIngredientBtn.setEnabled(true);
                    ingredientSet = true;
                }
                testIfCanSubmit();
            }
        });
        // Remove a line you could write an ingredient in
        removeIngredientBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!ingredientInputFields.isEmpty()) {
                    ingredientLinearLayout.removeView(ingredientInputFields.get(ingredientInputFields.size() - 1));
                    ingredientInputFields.remove(ingredientInputFields.size() - 1);
                    if (ingredientInputFields.isEmpty()) {
                        removeIngredientBtn.setEnabled(false);
                        ingredientSet = false;
                    }
                    testIfCanSubmit();
                }
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select an image"), REQUEST_IMAGE_GALLERY);
            }
        });

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        btnSubmitRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasConnectivity()) {

                    //new AsyncTask task
                    new postDinner().execute();


                }
                else {
                    Toast.makeText(getApplicationContext(), getString(R.string.errorNoNetworkConnection), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetSubmission();
            }
        });
    }


    public class postDinner extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://hemb.priv.no/privat/addDinner.php");

                //assemble data
                String name = txtRecipeName.getText().toString();
                String description = txtInstructions.getText().toString();
                String ingredients = "";

                for (int i = 0; i < ingredientInputFields.size(); i++) {
                    if ((String.valueOf(ingredientInputFields.get(i).getText()).trim().length() > 0)) {
                        ingredients += "-" + ingredientInputFields.get(i).getText().toString();
                    }
                }

                // Combine into Post request data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("name", name));
                nameValuePairs.add(new BasicNameValuePair("description", description));
                nameValuePairs.add(new BasicNameValuePair("ingredients", ingredients));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                //Retrieve response
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                String result11 = sb.toString();



                // parsing data
                return result11;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }



        }

        @Override
        protected void onPostExecute(String result) {

            //ON success
            Toast.makeText(getApplicationContext(), getString(R.string.recipeSubmitted), Toast.LENGTH_SHORT).show();
            resetSubmission();


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.isChecked()) {
            item.setChecked(false);
            canShake = false;
        }
        else {
            item.setChecked(true);
            canShake = true;
        }

        return true;
    }

    //After the user has found an image in gallery to use or canceled out, get data if something was chosen.
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (resCode == RESULT_OK) {
            if (reqCode == REQUEST_IMAGE_GALLERY) {
                //Load image from the uri into the ImageView in the create tab.
                Picasso.with(this).load(data.getData()).resize(120, 120).centerCrop().into(imgSubmit);
            } else if (reqCode == REQUEST_IMAGE_CAPTURE){
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                int height = imageBitmap.getHeight();
                int width = imageBitmap.getWidth();
                int insetTop = (height-width) / 2;
                imageBitmap = Bitmap.createBitmap(imageBitmap, 0, insetTop, width, width);
                ImageView mImageView = (ImageView)findViewById(R.id.imgSubmit);
                mImageView.setImageBitmap(Bitmap.createScaledBitmap(imageBitmap, 120, 120, true));
            }
            pictureSet = true;
            testIfCanSubmit();
        }
    }

    public void testIfCanSubmit() {

        if (pictureSet && recipeNameSet && instructionsSet && ingredientSet) {
            // Make sure there is text in the ingredients fields since they have no listener set
            for (int i = 0; i < ingredientInputFields.size(); i++) {
                if ((String.valueOf(ingredientInputFields.get(i).getText()).trim().length() <= 0)) {
                    btnSubmitRecipe.setEnabled(false);
                    return;
                }
            }
            btnSubmitRecipe.setEnabled(true);
        } else {
            btnSubmitRecipe.setEnabled(false);
        }
    }



    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void resetSubmission() {
        txtRecipeName.setText("");
        txtInstructions.setText("");
        while (ingredientInputFields.size() > 1) {
            ingredientLinearLayout.removeView(ingredientInputFields.get(ingredientInputFields.size() - 1));
            ingredientInputFields.remove(ingredientInputFields.size() - 1);
        }
        ingredientInputFields.get(0).setText("");
        imgSubmit.setImageResource(R.drawable.ic_launcher);
    }
}
