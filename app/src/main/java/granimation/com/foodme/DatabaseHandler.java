package granimation.com.foodme;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Vector;

/**
 * The db stores the url to the specific recipe as the unique key for each entry.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "FoodMe";
    private static final String TABLE_NAME = "favourite";
    private static final String KEY_ID = "recipeUrl";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
                        + KEY_ID + " TEXT PRIMARY KEY NOT NULL "
                        + ");"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    // Tries to add a new recipe, if it finds that the url already exists the db will reject it for not beeing unique
    public void add(String recipeUrl){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, recipeUrl);
        db.insert(TABLE_NAME, null, values);
        db.close();
    }
    public void remove(String recipeUrl){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, KEY_ID + " = " + "'" + recipeUrl + "'", null);
        db.close();
    }

    public int getCount(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCount = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null);
        mCount.moveToFirst();
        int count = mCount.getInt(0);
        db.close();
        return count;
    }

    public Vector<String> getAll(){
        Vector<String> favourites = new Vector<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        cursor.moveToFirst();
        do{
            Log.d("DBLog", "Read urls from DB: " + cursor.getString(0));
            favourites.add(cursor.getString(0));
        } while (cursor.moveToNext());
        db.close();
        return  favourites;
    }
}
